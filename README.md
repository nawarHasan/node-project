## About NODE.js API

The project's mission is to create Restful CRUD API with Node.js, Express and MongoDB.

### API Features

The application can create, read, update and delete data, for example: products, in a database. 
